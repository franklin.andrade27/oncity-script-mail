<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_0" id="Layout_0">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width:590px;">
                        <table width="100%" cellpadding="0" border="0" height="38" cellspacing="0">
                            <tbody><tr>
                                <td valign="top" height="38">
                                    <img width="20" height="38" style="display:block; max-height:38px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_1" id="Layout_1">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">
                                    <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0">
                                        <tbody><tr>
                                            <td valign="top" align="center">
                                                <table cellpadding="0" border="0" align="center" cellspacing="0" class="logo-img-center"> 
                                                    <tbody><tr>
                                                        <td valign="middle" align="center" style="line-height: 1px;">
                                                            <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block; " cellspacing="0" cellpadding="0" border="0"><div><img width="200" vspace="0" hspace="0" border="0" alt="OnCity" style="float: left;max-width:200px;display:block;" class="rnb-logo-img" src="https://img.mailinblue.com/2418373/images/rnb/original/5d56c16b81392229a65993aa.png"></div></div></td>
                                                    </tr>
                                                </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_" id="Layout_"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="content-spacing" style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="font-size:22px;"><b>Oi,&nbsp;</b> <strong><?php echo $nome; ?></strong>!</span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
            
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td class="content-spacing" style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="line-height:32px;"><span style="font-size:16px;"><span style="background-color: transparent;">Eu tenho certeza que você deve ter uma leve lembrança do OnCity! Até porque a última vez que </span><strong><u><?php echo $login; ?></u> </strong><span style="background-color: transparent;">esteve na cidade foi em</span>&nbsp;<strong><u><?php echo $ultimo_acesso ?> </u> </strong><span style="background-color: transparent;"> e você tinha <u><strong>$</strong></u></span><strong><u><?php echo $verba; ?> </u> </strong><span style="background-color: transparent;"> no bolso,<strong> <u>$</u></strong></span><u><strong><?php echo $banco; ?> </strong></u> n<span style="background-color: transparent;">o banco, tinha ações na Bovonc, tinha</span><strong><u>&nbsp;<?php echo $carisma; ?> </u> </strong><span style="background-color: transparent;"> de CARISMA e seu apetite estava em&nbsp;</span><u><strong><?php echo $apetite; ?> %</strong></u><span style="background-color: transparent;">!!!</span></span></div>

<div style="line-height:32px;"><span style="font-size:16px;">&nbsp;</span></div>

<div style="line-height:32px;"><span style="font-size:16px;"><strong>Bons tempos, não é verdade?</strong></span></div>

<div style="line-height:32px;"><span style="font-size:16px;">&nbsp;</span></div>

<div style="line-height:32px;"><span style="font-size:16px;">Até hoje, eu, o Fundador, recebo milhares (sem exagero...) de solicitações de convites mais de 10 anos depois que as luzes de OnCity se apagaram. Com o advento da colaboração digital e do crescimento pessoal de cada habitante da cidade, um antigo habitante sugeriu que fizéssemos crowdfunding para criar a cidade de novo. <u><a href="https://pt.wikipedia.org/wiki/Financiamento_coletivo" style="text-decoration: underline; color: rgb(0, 146, 255);">Se você não sabe o que é, clica aqui que tem uma visão geral.</a></u></span></div>

<div style="line-height:32px;">&nbsp;</div>

<div style="line-height:32px;"><span style="font-size:16px;">Fiquei animado com a ideia e pensei em perguntar pra você, um antigo e saudoso habitante da cidade, o que você acha da ideia e se você apoiaria o projeto.</span></div>

<div style="line-height:32px;"><span style="font-size:16px;">&nbsp;</span></div>

<div style="line-height:32px;"><span style="font-size:16px;">Os valores dos apoios seriam predefinidos e cada um viria com algumas recompensas físicas (tipo Citycard, por exemplo) e privilégios de acesso (que continuará precisando de convite para participar) ao novo OnCity, que tem como objetivo ser lançado esse ano ainda. A ideia é alcançar um valor total para viabilizar o projeto em tempo recorde. Caso não alcance esse total em 60 dias, o apoio seria devolvido inteirinho pra você sem ressentimentos :D<br>
<br>
Como você é uma pessoa que tinha <u><strong><?php echo $carisma; ?> </strong> de CARISMA </u>e&nbsp;<u><strong><?php echo $influencia; ?> &nbsp;</strong>de INFLUÊNCIA</u> , é obvio que eu te consultaria antes de fazer qualquer coisa!<br>
&nbsp;</span></div>

<div style="line-height:32px;"><span style="font-size:16px;">Somente pra eu ter uma noção, eu gostaria de saber:</span></div>

<hr></td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_6" id="Layout_6"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="content-spacing" style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><strong><span style="font-size:24px;">Até quanto vc estaria disposto a apoiar o projeto em uma plataforma de crowdfunding?</span></strong></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
            
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_10">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td class="content-spacing" style="font-size:14px; font-family:'Trebuchet MS',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: center;"><span style="font-size:18px"><span style="font-family:source sans pro,helvetica neue,helvetica,arial,sans-serif"><strong>VOCÊ NÃO VAI APOIAR NADA AGORA, BLZ? Isso é só uma pesquisa!</strong></span></span><br>
<br>
<span style="font-size:18px"><span style="font-family:source sans pro,helvetica neue,helvetica,arial,sans-serif">É só <strong>clicar </strong>no valor</span></span></div>

<div style="text-align: center;">&nbsp;</div>

<div style="text-align: center;"><a href="https://oncity.life/crowdfunding/?n=<?php echo $nome; ?> &amp;e=<?php echo $email; ?> &amp;v=R$10" style="font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; display: inline; width: 100%; line-height: normal; border-radius: 10px; height: 75px; text-decoration: none; color: rgb(216, 166, 43);"><span style="font-size: 12px;">R$</span> <span style="font-size: 30px; font-weight: bold; letter-spacing: -0.05em; margin-left: -6px;">10</span></a>&nbsp; &nbsp; &nbsp; &nbsp;<a href="https://oncity.life/crowdfunding/?n=<?php echo $nome; ?> &amp;e=<?php echo $email; ?> &amp;v=R$30" style="font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; display: inline; width: 100%; line-height: normal; border-radius: 10px; height: 75px; text-decoration: none; color: rgb(216, 166, 43);"><span style="font-size: 12px;">R$</span> <span style="font-size: 30px; font-weight: bold; letter-spacing: -0.05em; margin-left: -3px;">30</span></a>&nbsp; &nbsp; &nbsp;&nbsp;<a href="https://oncity.life/crowdfunding/?n=<?php echo $nome; ?> &amp;e=<?php echo $email; ?> &amp;v=R$50" style="font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; display: inline; width: 100%; line-height: normal; border-radius: 10px; height: 75px; text-decoration: none; color: rgb(216, 166, 43);"><span style="font-size: 12px;">R$</span> <span style="font-size: 30px; font-weight: bold; letter-spacing: -0.05em; margin-left: -3px;">50</span></a>&nbsp; &nbsp; &nbsp;&nbsp;<a href="https://oncity.life/crowdfunding/?n=<?php echo $nome; ?> &amp;e=<?php echo $email; ?> &amp;v=R$100" style="font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; display: inline; width: 100%; line-height: normal; border-radius: 10px; height: 75px; text-decoration: none; color: rgb(216, 166, 43);"><span style="font-size: 12px;">R$</span> <span style="font-size: 30px; font-weight: bold; letter-spacing: -0.05em; margin-left: -6px;">100</span></a> &nbsp;&nbsp;&nbsp;</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="0" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
            
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_12">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="0" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td class="content-spacing" style="font-size:14px; font-family:'Trebuchet MS',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: center;">&nbsp;</div>

<div style="text-align: center;"><a href="https://oncity.life/crowdfunding/?n=<?php echo $nome; ?> &amp;e=<?php echo $email; ?> &amp;v=Mais-que-R$100" style="font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; display: inline; width: 100%; line-height: normal; border-radius: 10px; height: 75px; text-decoration: none; color: rgb(216, 166, 43);"><span style="color:#003399;"><span style="font-size: 20px; font-weight: bold; text-transform: uppercase;">MAIS QUE R$100</span></span></a><span style="color:#0000FF;"> </span>&nbsp; &nbsp; &nbsp;<span style="color:#cc3300;"> </span><a href="https://oncity.life/crowdfunding/?n=<?php echo $nome; ?> &amp;e=<?php echo $email; ?> &amp;v=Nao-Apoiaria" style="font-family: &quot;source sans pro&quot;, &quot;helvetica neue&quot;, helvetica, arial, sans-serif; display: inline; width: 100%; line-height: normal; border-radius: 10px; height: 75px; text-decoration: none; color: rgb(216, 166, 43);"><span style="color:#cc3300;"><span style="font-size: 20px; font-weight: bold; text-transform: uppercase;">Não APOIARIA</span></span></a></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
            
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_9">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td class="content-spacing" style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div>
<div><span style="font-size:16px;">Como falei, isso aí é só uma pesquisa.<br>
<br>
<strong>Esse valor seria uma vez só, tá?</strong> Não é uma mensalidade ou assinatura. Só pra ajudar a viabilizar mesmo.</span></div>

<div><span style="font-size:16px;">&nbsp;</span></div>

<div><span style="font-size:16px;">Agradeço muito a participação e em breve te aviso sobre novidades, blz?</span></div>

<div><span style="font-size:16px;">Até mais, nobre amigo!</span></div>

<div><br>
<span style="font-size:16px;">Seu amigo, o Fundador 😉</span></div>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_" id="Layout_">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width:590px;">
                        <table width="100%" cellpadding="0" border="0" height="30" cellspacing="0">
                            <tbody><tr>
                                <td valign="top" height="30">
                                    <img width="20" height="30" style="display:block; max-height:30px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(249, 250, 252);">
                
                <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_3" id="Layout_3">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#f9fafc" style="min-width:590px; background-color: #f9fafc; text-align: center;">
                            <table width="590" class="rnb-container" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                                <tbody><tr>
                                    <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;"><div>This email was sent to <?php echo $email; ?> 
<div>You received this email because you are registered with OnCity some years ago!</div>

<div>&nbsp;</div>
</div>
</div>
                                        <div style="display: block; text-align: center;">
                                            <span style="font-size:14px; font-weight:normal; display: inline-block; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                                <a style="text-decoration:underline; color:#666666;font-size:14px;font-weight:normal;font-family:Arial,Helvetica,sans-serif;" target="_blank" href="{{ unsubscribe }}">Unsubscribe here</a></span>
                                        </div>
                                    </td></tr>
                                <tr>
                                    <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="text-align:center;">
                                            <a href="https://www.sendinblue.com/?utm_source=logo_mailin&amp;utm_campaign=14c9c680b61b8aa0f591a51367eabf9b&amp;utm_medium=email" target="_blank"><img border="0" hspace="0" vspace="0" width="129" height="48" alt="SendinBlue" style="margin:auto;" src="http://img.mailinblue.com/new_images/rnb/en.png"></a>
                                        </div></td>
                                </tr><tr>
                                    <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr></tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(249, 250, 252);">
                
                <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                            <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                                <tbody><tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                        <div>© 2019 OnCity</div>
                                    </td></tr>
                                <tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                
            </div></td>
    </tr></tbody></table>